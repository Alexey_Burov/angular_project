import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {UiComponent} from './ui/ui.component';
import {AdminComponent} from './admin/admin.component';
import {HeaderComponent} from './ui/header/header.component';
import {TilesComponent} from './ui/tiles/tiles.component';
import {MidComponent} from './ui/mid/mid.component';
import {MenuComponent} from './ui/menu/menu.component';
import {
  MatButtonModule,
  MatCardModule,
  MatGridListModule,
  MatMenuModule,
  MatToolbarModule,
  MatSidenavModule,
  MatCheckboxModule,

} from '@angular/material';
import {GeneralButtonComponent} from './ui/general-button/general-button.component';
import {ActiveTileComponent} from './ui/active-tile/active-tile.component';
import {Routes, RouterModule} from '@angular/router';
import {NotFoundComponent} from './not-found/not-found.component';
import {SidenavComponent} from './ui/sidenav/sidenav.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// определение маршрутов
const appRoutes: Routes = [
  {path: '', component: UiComponent},
  {path: 'admin', component: AdminComponent},
  {path: 'home', redirectTo: '', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  exports: [

  ],

  declarations: [
    ActiveTileComponent,
    AdminComponent,
    AppComponent,
    GeneralButtonComponent,
    HeaderComponent,
    MidComponent,
    MenuComponent,
    UiComponent,
    NotFoundComponent,
    SidenavComponent,
    TilesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    MatToolbarModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
