import { Component, OnInit } from '@angular/core';


export class Tile {
  head;
  bgImage;
  state;
}

@Component({
  selector: 'app-tiles',
  templateUrl: './tiles.component.html',
  styleUrls: ['./tiles.component.css']
})
export class TilesComponent implements OnInit {


  f = 'the sanatorium';
  s = 'the drink';
  t = 'the photos';
  c = 'the aercraft';

  bg = { bg: 'url("/images/sntr.gif")'};

  constructor() {

  }

  clickHover(id?: any) {
    console.log(id);
  }

  ngOnInit() {
  }

}
