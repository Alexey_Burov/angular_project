import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  opened: boolean;
  @Input() public events1: string[] = [];
  @Input() public events2: string[] = [];

  @Output() public visibleSideNav = new EventEmitter()
  constructor() {
    this.opened = false;
  }
  sendToParent() {
    this.visibleSideNav.emit(this.opened = !this.opened);
  }


  ngOnInit() {
  }
}
