import { Component, OnInit } from '@angular/core';

import SampleJson from '../../../images/data-mid.json'

@Component({
  selector: 'app-mid',
  templateUrl: './mid.component.html',
  styleUrls: ['./mid.component.css']
})
export class MidComponent implements OnInit {

  head = SampleJson.head.head;
  subhead = SampleJson.head.subhead;
  content = SampleJson.content.content;
  clause1 = SampleJson.clause[0];
  clause2 = SampleJson.clause[1];
  clause3 = SampleJson.clause[2];

  constructor() { }

  ngOnInit() {
  }

}
