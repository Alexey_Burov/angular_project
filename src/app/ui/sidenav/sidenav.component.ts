import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  @Input() public events1: string[] = [];
  @Input() public events2: string[] = [];
  @Input() public shouldRun: boolean;


  constructor() {
    this.shouldRun = false;
  }


  ngOnInit() { }

  clickHandler(e: string) {
    console.log(e);
  }
}
