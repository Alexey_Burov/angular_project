import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveTileComponent } from './active-tile.component';

describe('ActiveTileComponent', () => {
  let component: ActiveTileComponent;
  let fixture: ComponentFixture<ActiveTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
