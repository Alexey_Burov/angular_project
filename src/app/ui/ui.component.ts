import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ui',
  templateUrl: './ui.component.html',
  styleUrls: ['./ui.component.css']
})
export class UiComponent implements OnInit {

  public opened: boolean;
  public events1: string[] = ['home', 'pages', 'portfolio', 'blog'];
  public events2: string[] = ['shortcodes', 'shop', 'find', 'user'];

  constructor() {
    this.opened = false;

  }

  setVisibleSideNav(evnt){
    this.opened = evnt;
  }

  ngOnInit() {
  }

}
