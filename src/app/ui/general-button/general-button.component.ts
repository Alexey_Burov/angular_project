import {Component, Input, OnInit} from '@angular/core';



@Component({
  selector: 'app-general-button',
  templateUrl: './general-button.component.html',
  styleUrls: ['./general-button.component.css'],
})
export class GeneralButtonComponent implements OnInit {

  @Input() public text: string;
  @Input() public clear?: boolean;


  btnstyle = 'dark';

  constructor() { }

  ngOnInit() {
    console.log(this.clear);
    if(this.clear) {
      this.btnstyle = 'clear';
    }

  }

}

